// Include libaries
#include <stdio.h>
#include <stdlib.h>
// include header file
#include "header.h"

// create a function called convertOut
void convertOut() {

  // make the var lSize equal to width*height
  lSize =width*height;
  lSize = lSize*pMultiple;
  // allocate memory to contain the whole file:
  buffer = (unsigned char*) malloc (sizeof(char)*lSize);
  // copy the file into the buffer:
  fread (buffer,1,lSize,in_file);

  // print the magic number to output file
  fprintf(out_file, "P%d\n", pNumber);
  // print comment line of header to output file
  fprintf(out_file, "#This file is being converted from binary to ASCII\n");
  // print width and height line of header to output file
  fprintf(out_file, "%d %d\n", width, height);
  // print colour depth line of header to output file
  fprintf(out_file,"%d\n",depth);

  // Create a variable called i as an integer
  int i;
  // do a loop to print every char of the file stored in the buffer
  for (i = 0; i < lSize; i++)
  {
   fprintf(out_file, "%d ", *(buffer+i));
  }
// close the file
fclose(in_file);
// close the file stream
fclose(out_file);

} //end convertOut Function
