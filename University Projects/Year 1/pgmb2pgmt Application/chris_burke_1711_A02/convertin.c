// Include libaries
#include <stdio.h>
#include <stdlib.h>
// include header file
#include "header.h"

// Create a function called convertin
void convertIn() {

// Scan the header contents of the file
//scan the magic line of the header
fscanf(in_file,"%s\n",magicNumber);
//scan the comment line of the header
fgets(comment,sizeof(comment), in_file);
//scan the width and height of the header
fscanf(in_file,"%d %d\n", &width, &height);
//scan the colour depth of the header
fscanf(in_file,"%d\n",&depth);

if (magicNumber[1] == '5')
{
pNumber = 2;
pMultiple = 1;
}
else if (magicNumber[1] == '6')
{
  pNumber = 3;
  pMultiple = 3;
}

}// end of convertineader to output file
