//-----------------------------------------------------------------------------
//                 M A I N  F I L E
//-----------------------------------------------------------------------------
// Include libaries
#include <stdio.h>
#include <stdlib.h>
// include header file
#include "header.h"

//------------------------------------------------------------------------------
// Main Function - Where the program begins
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]){

// If user inputted 1 argument display error message
  if (argc == 1) {
    printf("User inputted insufficent amount of arguments\n");
    return -1;
  }

  // If user inputted 2 arguments print to terminal
  else if (argc == 2) {
    in_file = fopen(argv[1],"rwb");
    out_file = stdout;
    // call convert in function
    convertIn();
    // call convert out function
    convertOut();
    printf("\n");
  }

  // If user inputted 3 arguments print to output file
  else if (argc == 3) {
    in_file = fopen(argv[1],"rb");
    out_file = fopen(argv[2], "w");
    // call convert in function
    convertIn();
    // call convert out function
    convertOut();
    //notify user that file was successfuly written
    printf("%s ", argv[1]);
    printf("file successfully written to ");
    printf("%s", argv[2]);
    printf("\n");
  }

  // If user inputted too many arguments display error message
  else {
    printf("Too many arguments entered\n");
    return -1;
  }

// check if the input file was opened successfully
if(in_file==NULL){
      printf("Cannot open input txt\n");
      exit(1);
    }

// check if the output file was opened successfully
if(out_file==NULL){
      printf("Cannot open output txt\n");
      exit(1);
        }

// return success
return 0;
}
//------------------------------------------------------------------------------
