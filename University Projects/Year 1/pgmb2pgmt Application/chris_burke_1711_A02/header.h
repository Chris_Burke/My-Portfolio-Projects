//-----------------------------------------------------------------------------
//                 H E A D E R  F I L E
//-----------------------------------------------------------------------------
//declare variables
//declare string variable for magic number line of header
char magicNumber[3];
//declare string variable for comment line of header
char comment[1000];
//declare integer variable for width of file
int width;
//declare integer variable for height of file
int height;
//declare integer variable for colour depth of file
int depth;
//declare integer variable for pixel number  of file
int pNumber;
//declare integer variable for pixel Multiple of file
int pMultiple;
//declare long  variable for length size of file
long lSize;
//declare vinteger unsigned char pointer to buffer
unsigned char * buffer;
//file pointer Input
FILE *in_file;
//file pointer Output
FILE *out_file;
//decalare functions
//convert input function
void convertInput();
//convert outpyt function
void convertOnput();
