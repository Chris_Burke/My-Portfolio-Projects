COMP1711 - Procedural Programming - Coursework 1 - Deadline Novemeber 4th 10AM

                                ##A01_Part01##
1.Created switch statement with every letter uppercase/lowercase, numbers, and some basic punctuation symbols.
  
2. I created variables for userinput/output (lines1-7)

3. I concatenated the ascii character with my line variable dependant on what the character was, e.g. if the user typed cat it would print line one of cat up to line 7.

4. I added comments throughout my code to keep it clear and concise.

5. I also added a counter to the code which counted the amount of characters the user typed.

6. I created a for loop that ran through every letter of the user input printing out each character.


                                ##A01_Part02##
1. I created a counter variable for bold and italic.

2. I created a algorithmn for italic which spaces (lines1-7) if '&' and 'I' was pressed, and if '&' and 'i' was pressed it would toggle italic off.

3. I created another switch statement for bold which contained every character in asterisks in bold,  which was activated when the user typed '&' and 'B' this could be toggled off by pressing '&' and 'b'.

4. I also created an if statement for printing out the & symbol if a user pressed '&' and '&'.

5. I added comments throughout my code to keep it clear and concise.



                                ##A01_Part03##
1. I created a counter variable for uppercase and lowercase.

2. I included multiple if statements which were triggered when a user pressed '&' and 'U' and enabled the user to toggle on uppercasing every letter of the string, I also enabled the user to toggle this off by pressing '&' and 'u'.

3.I added multiple if statements that enabled the user to make every character lowercase using the integer ascii character and adding or minusing the value dependant on what character the user inputted, this would trigger when the user typed '&' and 'L'. I also added the abillity for the user to toggle lowercasing every character off by the user inputting '&' and 'l'.

4. I then created multiple if statements that allowed the user to capitalise the first character of every word in the string, this was performed by the user pressing '&' and 'C' triggering a counter which would be set to 1 and then running the counters code, in this case checking to see if the current character was equal to a letter in the ascii range and if it the next character along was equal to space, if these conditions were met the user would be able to turn the toggle on, I also added a way out of capitalising the first letter of every word by the user typing '&' and 'c' which would reset the counter to 0.

5. I added comments throughout the code making the code more understandable by the reader.




