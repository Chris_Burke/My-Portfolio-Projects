//include the standard I/O Libary
#include <stdio.h>
//Include the string libary
#include <string.h>
//Include
#include <unistd.h>
//Include
#include <stdlib.h>
//Main process
int main() {

//Declare variables
char line_1[999] = "     ";
char line_2[999] = "     ";
char line_3[999] = "     ";
char line_4[999] = "     ";
char line_5[999] = "     ";
char line_6[999] = "     ";
char line_7[999] = "     ";

//Italic TOGGLE counter
int iCount = 0;

//BOLD TOGGLE counter
int bCount = 0;

//Capitalise letter counter
int capCount = 0;

//Uppercase TOGGLE Counter
int uprCount = 0;

//Lowercase TOGGLE Counter
int lwrCount = 0;

//create a variable to count the userinput
int userInputCount =0;

//create an integer called "i" which is equal to 0
int i = 0;

//User input variable
char userInput[30];

//print greeting and control text
printf("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲\n");
printf("      Text Banner Program - Created By Christopher Burke 05/11/2015         ▲\n");
printf("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲\n");
printf("▲  Welcome to my program that transforms text into asterisk text banners.   ▲\n");
printf("▲To use this program simply type some text into the terminal and press enter▲\n");
printf("▲                  To end this program, press return.                       ▲\n");
printf("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲\n");
printf("▲ C O M M A N D S  - Below are some commands you can use in this version    ▲\n");
printf("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲\n");
printf("▲ |&B Enable Bold | &b Disable Bold | &I Enable Italic | &i Disable Italic| ▲\n");
printf("▲  | && print & symbol | &U Uppercase enabled | &u Uppercase  disabled      ▲\n");
printf("▲      | &C Enable Capitalise first letter of every word |                  ▲\n");
printf("▲     | &c Disable Capitalise first letter of every word |                  ▲\n");
printf("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲\n");

//Loop code
while (strcmp(userInput, "\n") !=0) {

  //creates a blank space between fgets and output
  if (strcmp(userInput, "\n")  !=0) {
    printf("\n");
}
  //Print the statement out "Please enter text"
  if (strcmp(userInput, "\n")  !=0) {
  printf("Please enter some text or press return to end program.\n");
}
//get the user input(user input, size of user input(100 characters)(using standard libary)
fgets(userInput,sizeof(userInput),stdin);

//creates a blank space between fgets and output
if (strcmp(userInput, "\n")  !=0) {
  printf("\n");
}
// using a loop if i is equal to 0 and less than userInput's length, increment i by 1
for (i = 0; i< strlen(userInput); i ++ ) {


//------------------------------------------------------------------------------
//-------------- W O R D  C A P I T A L I S A T I O N --------------------------
//------------------------------------------------------------------------------
if ((userInput[i]) == '&' && (userInput[i+1]) == 'C' && capCount ==0) {
i++;
i++;
capCount=1;
}

if ((capCount) == 1 && (userInput[i]) >= 97 && (userInput[i]) <= 122) {
  userInput[i]-=32;
  capCount=2;
  userInput[i]==0;
}

else if ((capCount) == 1 && (userInput[i]) < 97 && (userInput[i]) > 122) {
capCount=2;
}

if ((capCount) == 2 && (userInput[i-1]) == ' ' && (userInput[i]) >= 97 & (userInput[i]) <= 122) {
  userInput[i]-=32;
  capCount=2;
  userInput[i]==0;
}

if ((userInput[i]) == '&' && (userInput[i+1]) == 'c' && capCount ==2) {
i++;
i++;
capCount=0;
}
//------------------------------------------------------------------------------
//--------------U P P E R C A S E  &  L O W E R C A S E-------------------------
//------------------------------------------------------------------------------

//----------------------U P P E R C A S E --------------------------------------

//if current character is & and next character is U and Upr count is 0 then...
if ((userInput[i]) == '&' && (userInput[i+1]) == 'U' && uprCount ==0) {
//jump two characters ahead
++i;
++i;
//turn upr counter on
uprCount=1;
}
//if current character is & and next character is u and Upr count is 1 then...
if ((userInput[i]) == '&' && (userInput[i+1]) == 'u' && uprCount ==1) {
//jump two characters ahead
++i;
++i;
//turn upr counter off
uprCount=0;
}
//if upr counter is on and current character is less than or = to 97 on
//the asci chart and current character is greater than or equal o 122 on
//the asci chart, do this...
if ((uprCount) == 1  && (userInput[i]) >=97 && (userInput[i]) <=122) {
  //current character asci value is incremented by 32
  userInput[i]-=32;
}
//if upr counter is off
if ((uprCount) == 0  ) {
//current character's  ascii value is reset to 0
  userInput[i]==0;
}
//----------------------- L O W E R C A S E ------------------------------------

    if ((userInput[i]) == '&' && (userInput[i+1]) == 'L' && lwrCount ==0) {
    ++i;
    ++i;
    lwrCount=1;
    }

  if ((userInput[i]) == '&' && (userInput[i+1]) == 'l' && lwrCount ==1) {
  ++i;
  ++i;
  lwrCount=0;
}
      if (lwrCount ==1 & (userInput[i]) >=65 && (userInput[i]) <=90) {
        userInput[i]+=32;
      }

      if ((lwrCount) == 0  ) {
        userInput[i]==0;
      }
//------------------------------------------------------------------------------
//----------------------S Y M B O L (&  &)  C O D E-----------------------------
//------------------------------------------------------------------------------
    //&& Symbol Code
    //IF user input current character is equal to & and the next character
    //is equal to B then do this.
      if ((userInput[i]) == '&' && (userInput[i+1]) == '&') {
  (userInput[i+2]);
  strcat(line_1, " *** ");
  strcat(line_2, "** **");
  strcat(line_3, " **  ");
  strcat(line_4, "* ***");
  strcat(line_5, "*  * ");
  strcat(line_6, "*   *");
  strcat(line_7, " ** *");
}
//------------------------------------------------------------------------------
//----------------------B O L D   C O D E---------------------------------------
//------------------------------------------------------------------------------
//Bold CODE TOGGLE ON
//IF user input current character is equal to & and the next character
//is equal to B then do this.
if ((userInput[i]) == '&' && (userInput[i+1]) == 'B') {
//set bCount to 1
bCount=1;
//increment i twice
++i;
++i;
}

//BOLD CODE TOGGLED OFF
//IF user input current character is equal to & and the next character
//is equal to b then do this.
if ((userInput[i]) == '&' && (userInput[i+1]) == 'b') {
  //increment i twice
  ++i;
  ++i;
  //set bCount to 0
bCount=0;
}
//------------------------------------------------------------------------------
//----------------------I T A L I C  C O D E------------------------------------
//------------------------------------------------------------------------------
//ITALIC CODE TOGGLE ON
//IF user input current character is equal to & and the next character
//is equal to I then do this.
  if ((userInput[i]) == '&' && (userInput[i+1]) == 'I' && iCount == 0) {
//increment i twice
++i;
++i;
//Concatenate string 2 into lines(1-7)
strcat(line_1, "       ");
strcat(line_2, "      ");
strcat(line_3, "     ");
strcat(line_4, "    ");
strcat(line_5, "   ");
strcat(line_6, "  ");
strcat(line_7, " ");
//set intalic counter to 1
iCount = 1;
}

//ITALIC CODE TOGGLE OFF
//IF user input current character is equal to & and the next character
//is equal to i then do this.
  else if ((userInput[i]) == '&' && (userInput[i+1]) == 'i' && iCount == 1) {
//increment i twice
++i;
++i;
//Concatenate string 2 into lines(1-7)
strcat(line_1, " ");
strcat(line_2, "  ");
strcat(line_3, "   ");
strcat(line_4, "    ");
strcat(line_5, "     ");
strcat(line_6, "      ");
strcat(line_7, "       ");
//set intalic counter to 0
iCount = 0;
}
//------------------------------------------------------------------------------
//---------------------B O L D  C H A R A C T E R S-----------------------------
//------------------------------------------------------------------------------
if (bCount==1){
  switch (userInput[i]){
  //Uppercase A
  case 'A':
  strcat(line_1, "****** ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "****** ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase B
  case 'B':
  strcat(line_1, "*****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "*****  ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "*****  ");
  break;
  //Uppercase C
  case 'C':
  strcat(line_1, " ***   ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**     ");
  strcat(line_4, "**     ");
  strcat(line_5, "**     ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ***   ");
  break;
  //Uppercase D
  case 'D':
  strcat(line_1, "*****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "*****  ");
  break;
  //Uppercase E
  case 'E':
  strcat(line_1, "****** ");
  strcat(line_2, "**     ");
  strcat(line_3, "**     ");
  strcat(line_4, "****** ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "****** ");
  break;
  //Uppercase F
  case 'F':
  strcat(line_1, "****** ");
  strcat(line_2, "**     ");
  strcat(line_3, "**     ");
  strcat(line_4, "****** ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "**     ");
  break;
  //Uppercase G
  case 'G':
  strcat(line_1, " ***   ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**     ");
  strcat(line_4, "**  ** ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Uppercase H
  case 'H':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "****** ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase I
  case 'I':
  strcat(line_1, "****** ");
  strcat(line_2, "  **   ");
  strcat(line_3, "  **   ");
  strcat(line_4, "  **   ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "****** ");
  break;
  //Uppercase J
  case 'J':
  strcat(line_1, "  **** ");
  strcat(line_2, "    ** ");
  strcat(line_3, "    ** ");
  strcat(line_4, "    ** ");
  strcat(line_5, "*   ** ");
  strcat(line_6, "*   ** ");
  strcat(line_7, " ****  ");
  break;
  //Uppercase K
  case 'K':
  strcat(line_1, "**   * ");
  strcat(line_2, "**  *  ");
  strcat(line_3, "** *   ");
  strcat(line_4, "***    ");
  strcat(line_5, "** *   ");
  strcat(line_6, "**  *  ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase L
  case 'L':
  strcat(line_1, "**     ");
  strcat(line_2, "**     ");
  strcat(line_3, "**     ");
  strcat(line_4, "**     ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "****** ");
  break;
  //Uppercase M
  case 'M':
  strcat(line_1, "**   * ");
  strcat(line_2, "*** ** ");
  strcat(line_3, "*** ** ");
  strcat(line_4, "** * * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase N
  case 'N':
  strcat(line_1, "**   * ");
  strcat(line_2, "***  * ");
  strcat(line_3, "***  * ");
  strcat(line_4, "** * * ");
  strcat(line_5, "**  ** ");
  strcat(line_6, "**  ** ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase O
  case 'O':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Uppercase P
  case 'P':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "*****  ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "**     ");
  break;
  //Uppercase Q
  case 'Q':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "***  * ");
  strcat(line_6, " ****  ");
  strcat(line_7, "   *** ");
  break;
  //Uppercase R
  case 'R':
  strcat(line_1, "*****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "*****  ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase S
  case 'S':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**     ");
  strcat(line_4, " ****  ");
  strcat(line_5, "    * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Uppercase T
  case 'T':
  strcat(line_1, "****** ");
  strcat(line_2, "  **   ");
  strcat(line_3, "  **   ");
  strcat(line_4, "  **   ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Uppercase U
  case 'U':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Uppercase V
  case 'V':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "*** ** ");
  strcat(line_5, " ****  ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Uppercase W
  case 'W':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "** * * ");
  strcat(line_4, "** * * ");
  strcat(line_5, "*** ** ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase X
  case 'X':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "*** ** ");
  strcat(line_4, " ****  ");
  strcat(line_5, "*** ** ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Uppercase Y
  case 'Y':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, " ****  ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Uppercase Z
  case 'Z':
  strcat(line_1, "****** ");
  strcat(line_2, "    ** ");
  strcat(line_3, "   **  ");
  strcat(line_4, "  **   ");
  strcat(line_5, " **    ");
  strcat(line_6, "**     ");
  strcat(line_7, "****** ");
  break;
  //---------Lowercase Letters a-z -----------------------------------------------
  //Lowercase a
  case 'a':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "    ** ");
  strcat(line_5, " ***** ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ***** ");
  break;
  //Lowercase b
  case 'b':
  strcat(line_1, "**     ");
  strcat(line_2, "**     ");
  strcat(line_3, "*****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "*****  ");
  break;
  //Lowercase c
  case 'c':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**     ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase d
  case 'd':
  strcat(line_1, "    ** ");
  strcat(line_2, "    ** ");
  strcat(line_3, " ***** ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ***** ");
  break;
  //Lowercase e
  case 'e':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "****** ");
  strcat(line_6, "**     ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase f
  case 'f':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "*****  ");
  strcat(line_4, "**     ");
  strcat(line_5, "****   ");
  strcat(line_6, "**     ");
  strcat(line_7, "**     ");
  break;
  //Lowercase g
  case 'g':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, " ***** ");
  strcat(line_6, "    ** ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase h
  case 'h':
  strcat(line_1, "**     ");
  strcat(line_2, "**     ");
  strcat(line_3, "**     ");
  strcat(line_4, "****** ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Lowercase i
  case 'i':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "  **   ");
  strcat(line_4, "       ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Lowercase j
  case 'j':
  strcat(line_1, "       ");
  strcat(line_2, "    ** ");
  strcat(line_3, "       ");
  strcat(line_4, "    ** ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase k
  case 'k':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "**  *  ");
  strcat(line_4, "** *   ");
  strcat(line_5, "***    ");
  strcat(line_6, "** *   ");
  strcat(line_7, "**  *  ");
  break;
  //Lowercase l
  case 'l':
  strcat(line_1, "  **   ");
  strcat(line_2, "  **   ");
  strcat(line_3, "  **   ");
  strcat(line_4, "  **   ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase m
  case 'm':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "** * * ");
  strcat(line_5, "** * * ");
  strcat(line_6, "** * * ");
  strcat(line_7, "** * * ");
  break;
  //Lowercase n
  case 'n':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "**   * ");
  break;
  //Lowercase o
  case 'o':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase p
  case 'p':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "*****  ");
  strcat(line_6, "**     ");
  strcat(line_7, "**     ");
  break;
  //Lowercase q
  case 'q':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, " ***** ");
  strcat(line_6, "    ** ");
  strcat(line_7, "    ** ");
  break;
  //Lowercase r
  case 'r':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ****  ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "**     ");
  break;
  //Lowercase s
  case 's':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "*****  ");
  strcat(line_4, "**     ");
  strcat(line_5, "*****  ");
  strcat(line_6, "   **  ");
  strcat(line_7, "*****  ");
  break;
  //Lowercase t
  case 't':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "  **   ");
  strcat(line_4, " ****  ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  ** * ");
  strcat(line_7, "  ***  ");
  break;
  //Lowercase u
  case 'u':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase v
  case 'v':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "**   * ");
  strcat(line_4, " ** *  ");
  strcat(line_5, " ** *  ");
  strcat(line_6, " ** *  ");
  strcat(line_7, "  **   ");
  break;
  //Lowercase w
  case 'w':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "       ");
  strcat(line_4, "** * * ");
  strcat(line_5, "** * * ");
  strcat(line_6, "** * * ");
  strcat(line_7, " ****  ");
  break;
  //Lowercase x
  case 'x':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "**   * ");
  strcat(line_4, " ** *  ");
  strcat(line_5, "  **   ");
  strcat(line_6, " ** *  ");
  strcat(line_7, "**   * ");
  break;
  //Lowercase y
  case 'y':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "****** ");
  strcat(line_6, "    ** ");
  strcat(line_7, " ***** ");
  break;
  //Lowercase z
  case 'z':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "****** ");
  strcat(line_4, "    ** ");
  strcat(line_5, "  ***  ");
  strcat(line_6, "**     ");
  strcat(line_7, "****** ");
  break;
  //Space
  case ' ':
  strcat(line_1, "     ");
  strcat(line_2, "     ");
  strcat(line_3, "     ");
  strcat(line_4, "     ");
  strcat(line_5, "     ");
  strcat(line_6, "     ");
  strcat(line_7, "     ");
  break;
  //---------Numbers 0-9----------------------------------------------------------
  //Number 0
  case '0':
  strcat(line_1, "****** ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "**   * ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, "****** ");
  break;
  //Number 1
  case '1':
  strcat(line_1, "  **   ");
  strcat(line_2, " ***   ");
  strcat(line_3, "** *   ");
  strcat(line_4, "  **   ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "****** ");
  break;
  //Number 2
  case '2':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "    ** ");
  strcat(line_4, " ****  ");
  strcat(line_5, "**     ");
  strcat(line_6, "**     ");
  strcat(line_7, "****** ");
  break;
  //Number 3
  case '3':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "    ** ");
  strcat(line_4, " ****  ");
  strcat(line_5, "    ** ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Number 4
  case '4':
  strcat(line_1, "**   * ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, "****** ");
  strcat(line_5, "    ** ");
  strcat(line_6, "    ** ");
  strcat(line_7, "    ** ");
  break;
  //Number 5
  case '5':
  strcat(line_1, "****** ");
  strcat(line_2, "**     ");
  strcat(line_3, "**     ");
  strcat(line_4, "*****  ");
  strcat(line_5, "    ** ");
  strcat(line_6, "*   ** ");
  strcat(line_7, " ****  ");
  break;
  //Number 6
  case '6':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**     ");
  strcat(line_4, "*****  ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Number 7
  case '7':
  strcat(line_1, "****** ");
  strcat(line_2, "    ** ");
  strcat(line_3, "    ** ");
  strcat(line_4, "   **  ");
  strcat(line_5, "  **   ");
  strcat(line_6, " **    ");
  strcat(line_7, "**     ");
  break;
  //Number 8
  case '8':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, " ****  ");
  strcat(line_5, "**   * ");
  strcat(line_6, "**   * ");
  strcat(line_7, " ****  ");
  break;
  //Number 9
  case '9':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "**   * ");
  strcat(line_4, " ***** ");
  strcat(line_5, "    ** ");
  strcat(line_6, "    ** ");
  strcat(line_7, " ****  ");
  break;
  //---------Symbols--------------------------------------------------------------
  //To be typed up..
  //Symbol (!)
  case '!':
  strcat(line_1, "  **   ");
  strcat(line_2, "  **   ");
  strcat(line_3, "  **   ");
  strcat(line_4, "  **   ");
  strcat(line_5, "       ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Symbol (?)
  case '?':
  strcat(line_1, " ****  ");
  strcat(line_2, "**   * ");
  strcat(line_3, "   **  ");
  strcat(line_4, "  **   ");
  strcat(line_5, "       ");
  strcat(line_6, "  **   ");
  strcat(line_7, "  **   ");
  break;
  //Symbol (=)
  case '=':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, " ***** ");
  strcat(line_4, "       ");
  strcat(line_5, " ***** ");
  strcat(line_6, "       ");
  strcat(line_7, "       ");
  break;
  //Symbol (/)
  case '/':
  strcat(line_1, "       ");
  strcat(line_2, "    ** ");
  strcat(line_3, "   **  ");
  strcat(line_4, "  **   ");
  strcat(line_5, " **    ");
  strcat(line_6, "**     ");
  strcat(line_7, "       ");
  break;
  //Symbol (-)
  case '-':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "       ");
  strcat(line_4, "*******");
  strcat(line_5, "       ");
  strcat(line_6, "       ");
  strcat(line_7, "       ");
  break;
  //Symbol (+)
  case '+':
  strcat(line_1, "       ");
  strcat(line_2, "  **   ");
  strcat(line_3, "  **   ");
  strcat(line_4, "****** ");
  strcat(line_5, "  **   ");
  strcat(line_6, "  **   ");
  strcat(line_7, "       ");
  break;
  //Symbol (*)
  case '*':
  strcat(line_1, "**   * ");
  strcat(line_2, "*** ** ");
  strcat(line_3, " ****  ");
  strcat(line_4, "****** ");
  strcat(line_5, " ****  ");
  strcat(line_6, "*** ** ");
  strcat(line_7, "**   * ");
  break;
  //Symbol (")
  case '"':
  strcat(line_1, " **  * ");
  strcat(line_2, " **  * ");
  strcat(line_3, " **  * ");
  strcat(line_4, "       ");
  strcat(line_5, "       ");
  strcat(line_6, "       ");
  strcat(line_7, "       ");
  break;
  //Symbol (#)
  case '#':
  strcat(line_1, " ** *  ");
  strcat(line_2, " ** *  ");
  strcat(line_3, "****** ");
  strcat(line_4, " ** *  ");
  strcat(line_5, "****** ");
  strcat(line_6, " ** *  ");
  strcat(line_7, " ** *  ");
  break;
  //Symbol ($)
  case '$':
  strcat(line_1, " ***  ");
  strcat(line_2, "* * * ");
  strcat(line_3, "* *   ");
  strcat(line_4, "***** ");
  strcat(line_5, "  * * ");
  strcat(line_6, "* * * ");
  strcat(line_7, " ***  ");
  break;
  //Symbol (.)
  case '.':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "       ");
  strcat(line_4, "       ");
  strcat(line_5, "       ");
  strcat(line_6, "       ");
  strcat(line_7, " ***   ");
  break;
  //Symbol (,)
  case ',':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "       ");
  strcat(line_4, "       ");
  strcat(line_5, "       ");
  strcat(line_6, "  ***  ");
  strcat(line_7, "  **   ");
  break;
  //Symbol (
  case '(':
  strcat(line_1, "  **** ");
  strcat(line_2, "***    ");
  strcat(line_3, "**     ");
  strcat(line_4, "**     ");
  strcat(line_5, "**     ");
  strcat(line_6, "***    ");
  strcat(line_7, "  **** ");
  break;
  //Symbol )
  case ')':
  strcat(line_1, "****   ");
  strcat(line_2, "   *** ");
  strcat(line_3, "    ** ");
  strcat(line_4, "    ** ");
  strcat(line_5, "    ** ");
  strcat(line_6, "   *** ");
  strcat(line_7, "****   ");
  break;
  //Symbol (:)
  case ':':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "  ***  ");
  strcat(line_4, "       ");
  strcat(line_5, "       ");
  strcat(line_6, "  ***  ");
  strcat(line_7, "       ");
  break;
  //Symbol (;)
  case ';':
  strcat(line_1, "       ");
  strcat(line_2, "       ");
  strcat(line_3, "  ***  ");
  strcat(line_4, "       ");
  strcat(line_5, "       ");
  strcat(line_6, "  ***  ");
  strcat(line_7, "  **   ");
  break;
  //Symbol (>)
  case '>':
  strcat(line_1, "***    ");
  strcat(line_2, " ***   ");
  strcat(line_3, "  ***  ");
  strcat(line_4, "   *** ");
  strcat(line_5, "  ***  ");
  strcat(line_6, " ***   ");
  strcat(line_7, "***    ");
  break;
  //Symbol (<)
  case '<':
  strcat(line_1, "   *** ");
  strcat(line_2, "  ***  ");
  strcat(line_3, " ***   ");
  strcat(line_4, "***    ");
  strcat(line_5, " ***   ");
  strcat(line_6, "  ***  ");
  strcat(line_7, "   *** ");
  break;
} //switch statment for bold
}//If bCount==1

//------------------------------------------------------------------------------
//---------------------R E G U L A R  C H A R A C T E R S-----------------------
//------------------------------------------------------------------------------
//If statement that triggers conditions if bCount is equal to 0
if (bCount ==0){
//Switch cases to regular characters
switch (userInput[i]){
  //---------Uppercase letters A-Z ---------------------------------------------
  //Uppercase A
case 'A':
strcat(line_1, "***** ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "***** ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase B
case 'B':
strcat(line_1, "****  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "****  ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "****  ");
break;
//Uppercase C
case 'C':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*     ");
strcat(line_4, "*     ");
strcat(line_5, "*     ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Uppercase D
case 'D':
strcat(line_1, "****  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "****  ");
break;
//Uppercase E
case 'E':
strcat(line_1, "***** ");
strcat(line_2, "*     ");
strcat(line_3, "*     ");
strcat(line_4, "***** ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "***** ");
break;
//Uppercase F
case 'F':
strcat(line_1, "***** ");
strcat(line_2, "*     ");
strcat(line_3, "*     ");
strcat(line_4, "***** ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "*     ");
break;
//Uppercase G
case 'G':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*     ");
strcat(line_4, "*  ** ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " **** ");
break;
//Uppercase H
case 'H':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "***** ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase I
case 'I':
strcat(line_1, "***** ");
strcat(line_2, "  *   ");
strcat(line_3, "  *   ");
strcat(line_4, "  *   ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "***** ");
break;
//Uppercase J
case 'J':
strcat(line_1, "  *** ");
strcat(line_2, "    * ");
strcat(line_3, "    * ");
strcat(line_4, "    * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Uppercase K
case 'K':
strcat(line_1, "*   * ");
strcat(line_2, "*  *  ");
strcat(line_3, "* *   ");
strcat(line_4, "**    ");
strcat(line_5, "* *   ");
strcat(line_6, "*  *  ");
strcat(line_7, "*   * ");
break;
//Uppercase L
case 'L':
strcat(line_1, "*     ");
strcat(line_2, "*     ");
strcat(line_3, "*     ");
strcat(line_4, "*     ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "***** ");
break;
//Uppercase M
case 'M':
strcat(line_1, "*   * ");
strcat(line_2, "** ** ");
strcat(line_3, "** ** ");
strcat(line_4, "* * * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase N
case 'N':
strcat(line_1, "*   * ");
strcat(line_2, "**  * ");
strcat(line_3, "**  * ");
strcat(line_4, "* * * ");
strcat(line_5, "*  ** ");
strcat(line_6, "*  ** ");
strcat(line_7, "*   * ");
break;
//Uppercase O
case 'O':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Uppercase P
case 'P':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "****  ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "*     ");
break;
//Uppercase Q
case 'Q':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "**  * ");
strcat(line_6, " ***  ");
strcat(line_7, "   ** ");
break;
//Uppercase R
case 'R':
strcat(line_1, "****  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "****  ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase S
case 'S':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*     ");
strcat(line_4, " ***  ");
strcat(line_5, "    * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Uppercase T
case 'T':
strcat(line_1, "***** ");
strcat(line_2, "  *   ");
strcat(line_3, "  *   ");
strcat(line_4, "  *   ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Uppercase U
case 'U':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Uppercase V
case 'V':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "** ** ");
strcat(line_5, " ***  ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Uppercase W
case 'W':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "* * * ");
strcat(line_4, "* * * ");
strcat(line_5, "** ** ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase X
case 'X':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "** ** ");
strcat(line_4, " ***  ");
strcat(line_5, "** ** ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Uppercase Y
case 'Y':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, " ***  ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Uppercase Z
case 'Z':
strcat(line_1, "***** ");
strcat(line_2, "    * ");
strcat(line_3, "   *  ");
strcat(line_4, "  *   ");
strcat(line_5, " *    ");
strcat(line_6, "*     ");
strcat(line_7, "***** ");
break;
//---------Lowercase Letters a-z -----------------------------------------------
//Lowercase a
case 'a':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "    * ");
strcat(line_5, " **** ");
strcat(line_6, "*   * ");
strcat(line_7, " **** ");
break;
//Lowercase b
case 'b':
strcat(line_1, "*     ");
strcat(line_2, "*     ");
strcat(line_3, "****  ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "****  ");
break;
//Lowercase c
case 'c':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*     ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, " ***  ");
break;
//Lowercase d
case 'd':
strcat(line_1, "    * ");
strcat(line_2, "    * ");
strcat(line_3, " **** ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " **** ");
break;
//Lowercase e
case 'e':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, "***** ");
strcat(line_6, "*     ");
strcat(line_7, " ***  ");
break;
//Lowercase f
case 'f':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "****  ");
strcat(line_4, "*     ");
strcat(line_5, "***   ");
strcat(line_6, "*     ");
strcat(line_7, "*     ");
break;
//Lowercase g
case 'g':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, " **** ");
strcat(line_6, "    * ");
strcat(line_7, " ***  ");
break;
//Lowercase h
case 'h':
strcat(line_1, "*     ");
strcat(line_2, "*     ");
strcat(line_3, "*     ");
strcat(line_4, "***** ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Lowercase i
case 'i':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "  *   ");
strcat(line_4, "      ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Lowercase j
case 'j':
strcat(line_1, "      ");
strcat(line_2, "    * ");
strcat(line_3, "      ");
strcat(line_4, "    * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Lowercase k
case 'k':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "*  *  ");
strcat(line_4, "* *   ");
strcat(line_5, "**    ");
strcat(line_6, "* *   ");
strcat(line_7, "*  *  ");
break;
//Lowercase l
case 'l':
strcat(line_1, "  *   ");
strcat(line_2, "  *   ");
strcat(line_3, "  *   ");
strcat(line_4, "  *   ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, " ***  ");
break;
//Lowercase m
case 'm':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "* * * ");
strcat(line_5, "* * * ");
strcat(line_6, "* * * ");
strcat(line_7, "* * * ");
break;
//Lowercase n
case 'n':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "*   * ");
break;
//Lowercase o
case 'o':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Lowercase p
case 'p':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, "****  ");
strcat(line_6, "*     ");
strcat(line_7, "*     ");
break;
//Lowercase q
case 'q':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, " **** ");
strcat(line_6, "    * ");
strcat(line_7, "    * ");
break;
//Lowercase r
case 'r':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " ***  ");
strcat(line_4, "*   * ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "*     ");
break;
//Lowercase s
case 's':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "****  ");
strcat(line_4, "*     ");
strcat(line_5, "****  ");
strcat(line_6, "   *  ");
strcat(line_7, "****  ");
break;
//Lowercase t
case 't':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "  *   ");
strcat(line_4, " ***  ");
strcat(line_5, "  *   ");
strcat(line_6, "  * * ");
strcat(line_7, "  **  ");
break;
//Lowercase u
case 'u':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Lowercase v
case 'v':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "*   * ");
strcat(line_4, " * *  ");
strcat(line_5, " * *  ");
strcat(line_6, " * *  ");
strcat(line_7, "  *   ");
break;
//Lowercase w
case 'w':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "      ");
strcat(line_4, "* * * ");
strcat(line_5, "* * * ");
strcat(line_6, "* * * ");
strcat(line_7, " ***  ");
break;
//Lowercase x
case 'x':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "*   * ");
strcat(line_4, " * *  ");
strcat(line_5, "  *   ");
strcat(line_6, " * *  ");
strcat(line_7, "*   * ");
break;
//Lowercase y
case 'y':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "***** ");
strcat(line_6, "    * ");
strcat(line_7, " **** ");
break;
//Lowercase z
case 'z':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "***** ");
strcat(line_4, "    * ");
strcat(line_5, "  **  ");
strcat(line_6, "*     ");
strcat(line_7, "***** ");
break;
//Space
case ' ':
strcat(line_1, "     ");
strcat(line_2, "     ");
strcat(line_3, "     ");
strcat(line_4, "     ");
strcat(line_5, "     ");
strcat(line_6, "     ");
strcat(line_7, "     ");
break;
//---------Numbers 0-9----------------------------------------------------------
//Number 0
case '0':
strcat(line_1, "***** ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "*   * ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, "***** ");
break;
//Number 1
case '1':
strcat(line_1, "  *   ");
strcat(line_2, " **   ");
strcat(line_3, "* *   ");
strcat(line_4, "  *   ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "***** ");
break;
//Number 2
case '2':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "    * ");
strcat(line_4, " ***  ");
strcat(line_5, "*     ");
strcat(line_6, "*     ");
strcat(line_7, "***** ");
break;
//Number 3
case '3':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "    * ");
strcat(line_4, " ***  ");
strcat(line_5, "    * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Number 4
case '4':
strcat(line_1, "*   * ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, "***** ");
strcat(line_5, "    * ");
strcat(line_6, "    * ");
strcat(line_7, "    * ");
break;
//Number 5
case '5':
strcat(line_1, "***** ");
strcat(line_2, "*     ");
strcat(line_3, "*     ");
strcat(line_4, "****  ");
strcat(line_5, "    * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Number 6
case '6':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*     ");
strcat(line_4, "****  ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Number 7
case '7':
strcat(line_1, "***** ");
strcat(line_2, "    * ");
strcat(line_3, "    * ");
strcat(line_4, "   *  ");
strcat(line_5, "  *   ");
strcat(line_6, " *    ");
strcat(line_7, "*     ");
break;
//Number 8
case '8':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, " ***  ");
strcat(line_5, "*   * ");
strcat(line_6, "*   * ");
strcat(line_7, " ***  ");
break;
//Number 9
case '9':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "*   * ");
strcat(line_4, " **** ");
strcat(line_5, "    * ");
strcat(line_6, "    * ");
strcat(line_7, " ***  ");
break;
//---------Symbols--------------------------------------------------------------
//To be typed up..
//Symbol (!)
case '!':
strcat(line_1, "  *   ");
strcat(line_2, "  *   ");
strcat(line_3, "  *   ");
strcat(line_4, "  *   ");
strcat(line_5, "      ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Symbol (?)
case '?':
strcat(line_1, " ***  ");
strcat(line_2, "*   * ");
strcat(line_3, "   *  ");
strcat(line_4, "  *   ");
strcat(line_5, "      ");
strcat(line_6, "  *   ");
strcat(line_7, "  *   ");
break;
//Symbol (=)
case '=':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, " **** ");
strcat(line_4, "      ");
strcat(line_5, " **** ");
strcat(line_6, "      ");
strcat(line_7, "      ");
break;
//Symbol (/)
case '/':
strcat(line_1, "      ");
strcat(line_2, "    * ");
strcat(line_3, "   *  ");
strcat(line_4, "  *   ");
strcat(line_5, " *    ");
strcat(line_6, "*     ");
strcat(line_7, "      ");
break;
//Symbol (-)
case '-':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "      ");
strcat(line_4, "***** ");
strcat(line_5, "      ");
strcat(line_6, "      ");
strcat(line_7, "      ");
break;
//Symbol (+)
case '+':
strcat(line_1, "      ");
strcat(line_2, "  *   ");
strcat(line_3, "  *   ");
strcat(line_4, "***** ");
strcat(line_5, "  *   ");
strcat(line_6, "  *   ");
strcat(line_7, "      ");
break;
//Symbol (*)
case '*':
strcat(line_1, "*   * ");
strcat(line_2, "** ** ");
strcat(line_3, " ***  ");
strcat(line_4, "***** ");
strcat(line_5, " ***  ");
strcat(line_6, "** ** ");
strcat(line_7, "*   * ");
break;
//Symbol (")
case '"':
strcat(line_1, " *  * ");
strcat(line_2, " *  * ");
strcat(line_3, " *  * ");
strcat(line_4, "      ");
strcat(line_5, "      ");
strcat(line_6, "      ");
strcat(line_7, "      ");
break;
//Symbol (#)
case '#':
strcat(line_1, " * *  ");
strcat(line_2, " * *  ");
strcat(line_3, "***** ");
strcat(line_4, " * *  ");
strcat(line_5, "***** ");
strcat(line_6, " * *  ");
strcat(line_7, " * *  ");
break;
//Symbol ($)
case '$':
strcat(line_1, " ***  ");
strcat(line_2, "* * * ");
strcat(line_3, "* *   ");
strcat(line_4, "***** ");
strcat(line_5, "  * * ");
strcat(line_6, "* * * ");
strcat(line_7, " ***  ");
break;
//Symbol (.)
case '.':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "      ");
strcat(line_4, "      ");
strcat(line_5, "      ");
strcat(line_6, "      ");
strcat(line_7, " **   ");
break;
//Symbol (,)
case ',':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "      ");
strcat(line_4, "      ");
strcat(line_5, "      ");
strcat(line_6, "  **  ");
strcat(line_7, "  *   ");
break;
//Symbol (
case '(':
strcat(line_1, "  *** ");
strcat(line_2, "**    ");
strcat(line_3, "*     ");
strcat(line_4, "*     ");
strcat(line_5, "*     ");
strcat(line_6, "**    ");
strcat(line_7, "  *** ");
break;
//Symbol )
case ')':
strcat(line_1, "***   ");
strcat(line_2, "   ** ");
strcat(line_3, "    * ");
strcat(line_4, "    * ");
strcat(line_5, "    * ");
strcat(line_6, "   ** ");
strcat(line_7, "***   ");
break;
//Symbol (:)
case ':':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "  **  ");
strcat(line_4, "      ");
strcat(line_5, "      ");
strcat(line_6, "  **  ");
strcat(line_7, "      ");
break;
//Symbol (;)
case ';':
strcat(line_1, "      ");
strcat(line_2, "      ");
strcat(line_3, "  **  ");
strcat(line_4, "      ");
strcat(line_5, "      ");
strcat(line_6, "  **  ");
strcat(line_7, "  *   ");
break;
//Symbol (>)
case '>':
strcat(line_1, "**    ");
strcat(line_2, " **   ");
strcat(line_3, "  **  ");
strcat(line_4, "   ** ");
strcat(line_5, "  **  ");
strcat(line_6, " **   ");
strcat(line_7, "**    ");
break;
//Symbol (<)
case '<':
strcat(line_1, "   ** ");
strcat(line_2, "  **  ");
strcat(line_3, " **   ");
strcat(line_4, "**    ");
strcat(line_5, " **   ");
strcat(line_6, "  **  ");
strcat(line_7, "   ** ");
break;
} //close if statement for if bCount=0
}
 }

//Print the lines of code out
if (strcmp(userInput, "\n")  !=0) {
printf("%s \n", line_1);
printf("%s \n", line_2);
printf("%s \n", line_3);
printf("%s \n", line_4);
printf("%s \n", line_5);
printf("%s \n", line_6);
printf("%s \n", line_7);
}
//user input count is equal to string length lf user input -1 for newline
userInputCount += strlen(userInput) -1;
//print statements for testing purposes
//printf("%s \n", userInput);
//printf("%d \n", userInputCount);
//reset user input
strcpy(line_1,"");
strcpy(line_2,"");
strcpy(line_3,"");
strcpy(line_4,"");
strcpy(line_5,"");
strcpy(line_6,"");
strcpy(line_7,"");
}//close while loop
//print the toal number of characters processed
printf("The total number of characters processed are: %d \n", userInputCount, " Characters.");
return 0;
}
