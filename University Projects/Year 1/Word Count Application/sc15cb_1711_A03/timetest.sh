# Shell Script

# I'm using this shell script with my example text file

# however you can modify it by simply changing "example.txt" to whatever
# filename you would like to test.

# Test 1. Linked List.
# This will test the elapsed time of my program running the Linked List
# approach on my provided textfile "example.txt"
echo "============================"
echo "Testing Linked list"
time ./getWords example.txt > /dev/null
# Create a seperator to seperate the tests
echo "============================"

#-------------------------------------------------------------------------------

# Test 2. Sorted Linked List.
# This will test the elapsed time of my program running the Sorted
# Linked List approach on my provided textfile "example.txt"
echo "Testing Sorted Linked list"
time ./getWords example.txt sort > /dev/null
echo "============================"
