///////////////////////////////////////////
//                                       //
//   COMP 1711 Novemeber 2015            //
//                                       //
//Functions.c: code file for all the     //
//             functions used within my  //
//             program                   //
//                                       //
///////////////////////////////////////////

// include stdlib for malloc(), string for strdup(), stdio for printing
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// include the header file
#include "header.h"

////////////////////////////////////
//          filter()              //
////////////////////////////////////

// transform uppercase letters to lowercase letters, and deal with apostrophes
void filter()
	{ //filter()
	int i;
	// for loop that runs through the buffer
	for(i = 0; i < lSize; i++) { // loops through buffer

		// Case 1 removing Uppercase letters
		if ((buffer[i]) >=65 && (buffer[i]) <=90) { // if its a Capital letter
		// make it a lowercase letter
			buffer[i] +=32;
			} // if its a capital letter

		//-----------------------------------------------------//
		// Case 2 ‘ (apostrophe) is at the end of a word       //
		//-----------------------------------------------------//
		if ((buffer[i]) ==39 && (buffer[i+1]) ==32 && (buffer[i-1]) >= 97 && (buffer[i-1]) <=122) { // ‘ (apostrophe) at end of a word
		// turns the ‘ (apostrophe) into a full stop and removes it later
			buffer[i]+=14;
			} // ‘ (apostrophe) at the end of a word

		//-----------------------------------------------------//
		// Case 3 ‘ (apostrophe) is at the beginning of a word //
		//-----------------------------------------------------//
		// if the current character is an apostrophe and the next character is a letter
		// and the previous letter is a space, then transform the current character into a full stop.
		if ((buffer[i]) ==39 && (buffer[i-1]) ==32 && (buffer[i+1]) >= 97 && (buffer[i+1]) <=122) { // ‘ (apostrophe) at the beginning of the word
		// turns the ‘ (apostrophe) into a full stop and removes it later
			buffer[i]+=14;
		} // ‘ (apostrophe) at the beginning of a word

	} // loops through the buffer
} // filter()

////////////////////////////////////
//          MakeOutput()          //
////////////////////////////////////
void MakeOutput() { // Make Output()

	// print a new line to make UI more readable
	printf("\n");

	// Close file and delete allocated memory
	fclose(in_file);
	free(buffer);
} // MakeOutput()

////////////////////////////////////
//          MakeBuffer()          //
////////////////////////////////////
void MakeBuffer() { // MakeBuffer()

	if(in_file==NULL) { // if no file is found display an error
		fputs("ERROR - No File found\n", stderr);
		exit(1);
	} // if no file is found display an error

	// obtain file size
	fseek(in_file, 0, SEEK_END);
	// create a variable called lSize that is equal to the size of the file
	lSize = ftell(in_file);
	// reset the file pointer to the beginning of the file
	rewind(in_file);

	// allocate memory to contain the whole file:
	buffer = (unsigned char*) malloc(sizeof(char)*lSize);
	if(buffer ==NULL) { // if the buffer is empty display an error
		fputs("ERROR - Memory Error", stderr);
		exit(2);
	} // if the buffer is empty display error

	// read the file soure
	fread(buffer, lSize, 1, in_file);

} // MakeBuffer()

////////////////////////////////////
//          CreateFilledNode()    //
////////////////////////////////////

// Create a node with a specific name / amount
// by *COPYING* the parameter: The calling function
// is responsible for its own strings
WordNode *CreateFilledNode(char *wordor, int owing)
	{ // CreateFilledNode()
	// first allocate the memory for it
	WordNode *newNode = (WordNode *) malloc(sizeof(WordNode));

	// copy the values into it
	newNode->name = strdup(wordor);
	newNode->amount = owing;

	// and set the next pointer to NULL
	newNode->nextWord = NULL;

	// and return the node
	return newNode;
	} // CreateFilledNode()

////////////////////////////////////
//          PrintNode()           //
////////////////////////////////////

// Print out a node
void PrintNode(WordNode *wordNode)
	{ // PrintNode()
	printf("%20s  | Occured: %d\n", wordNode->name, wordNode->amount);
	} // PrintNode()

////////////////////////////////////
//          DeleteNode()          //
////////////////////////////////////

// Deleta a single node
void DeleteNode(WordNode *wordNode)
	{ // DeleteNode()
	// get rid of the node's string
	free(wordNode->name);

	// then get rid of the node
	free(wordNode);
	} // DeleteNode()

////////////////////////////////////
//          InitialiseList()      //
////////////////////////////////////

// Initialise the list to empty
void InitialiseList(WordList *wordList)
	{ // InitialiseList()
	// set the node pointer to empty
	wordList->topNode = NULL;
	} // InitialiseList()

////////////////////////////////////
//          CountNodes()          //
////////////////////////////////////

// Find out how many nodes in the list
int CountNodes(WordList *wordList)
	{ // CountNodes()
	// variable for counting
	int nNodes = 0;

	// a node to use for walking down the list
	WordNode *walkNode;

	// for loop with the three statements shown a different lines
	for (	walkNode = wordList->topNode;		// initialise to the first node
			walkNode != NULL;					// stop when we hit NULL
			walkNode = walkNode->nextWord )		// increment by taking next pointer
		{ // for loop body
		nNodes++;
		} // for loop body

	// return the total count
	return nNodes;
	} // CountNodes()

////////////////////////////////////
//          PrintList()           //
////////////////////////////////////

// Print out list
void PrintList(WordList *wordList)
	{ // PrintList()
	// a node to use for walking down the list
	WordNode *walkNode = wordList->topNode;

	printf("=====================\n");
	printf("\n");

	// for a loop with empty initialisation (see previous line of code)
	for ( ; walkNode != NULL; walkNode = walkNode->nextWord )
		{ // for loop body
		PrintNode(walkNode);
		} // for loop body

	// print a couple of lines first
	printf("=====================\n");
	printf("Total Unique Words is: %d\n", CountNodes(wordList));
	printf("=====================\n");

	} // PrintList()

////////////////////////////////////
//             AddNode()          //
////////////////////////////////////

// Add a node to the top of the list
void AddNode(WordList *wordList, WordNode *wordNode)
	{ // AddNode()
	// add the existing list to the bottom of the new one
	wordNode->nextWord = wordList->topNode;

	// and set the list to point to the new one
	wordList->topNode = wordNode;
	} // AddNode()

////////////////////////////////////
//           FindByKey()          //
////////////////////////////////////

// Find a specific node in the list by key
WordNode *FindByKey(WordList *wordList, char *keyName)
	{ // FindByKey()
	// pointer to use to find the node
	WordNode *keyNode = wordList->topNode;

	// use a while loop to find the key
	while (keyNode !=NULL)
		{ // while loop
		// use string comparison on key and name
		if (strcmp(keyNode->name, keyName)==0)
			{ // found key
			keyNode->amount +=1;
			return keyNode;
			} // found key

		// move to next node
		keyNode = keyNode->nextWord;
		} // while loop

	AddNode(wordList, CreateFilledNode(keyName, 1));;

	// if we get here, keyNode is NULL, so return it
	return keyNode;
	} // FindByKey()

	////////////////////////////////////
	//           SortedFindByKey()    //
	////////////////////////////////////

	// Find a specific node in the list by key
	WordNode *SortedFindByKey(WordList *wordList, char *keyName)
		{ // SortedFindByKey()
		// pointer to use to find the node
		WordNode *keyNode = wordList->topNode;

		// use a while loop to find the key
		while (keyNode !=NULL)
			{ // while loop
			// use string comparison on key and name
			if (strcmp(keyNode->name, keyName)==0)
				{ // found key
				keyNode->amount +=1;
				return keyNode;
				} // found key

			// move to next node
			keyNode = keyNode->nextWord;
			} // while loop

		AddNodeSorted(wordList, CreateFilledNode(keyName, 1));

		// if we get here, keyNode is NULL, so return it
		return keyNode;
	} // SortedFindByKey()

////////////////////////////////////
//      Special Cases At Top      //
////////////////////////////////////

// Deletea node in the list
void DeleteNodeFromList(WordList *wordList, WordNode *wordNode)
	{ // DeleteNodeFromList()
	// start a pointer at the top
	WordNode *walkNode = wordList->topNode;

	// if the node to delete is the top node
	if (walkNode == NULL)
		{ // no list at all
		return;
		} // no list at all

	// if the node to delete is the top node
	if (walkNode == wordNode)
		{ // top of list
		// wordNode holds the one to delete
		// so just move the tail end to be the top node
		wordList->topNode = wordNode->nextWord;

		// be paranoid and set wordNode to point to NULL
		wordNode->nextWord = NULL;

		// now delete it
		DeleteNode(wordNode);

		// we're done
		return;
		} // top of list

	// OK, there's at least one node, so loop until the next
	// node is the right one
	while (walkNode != NULL)
		{ // walking down the list
		if (walkNode->nextWord == wordNode)
			{ // found it
			// again, just change the connections
			walkNode->nextWord = wordNode->nextWord;

			// clean up wordNode & Delete
			wordNode->nextWord = NULL;
			DeleteNode(wordNode);

			// now return
			return;
			} // found it

		// Keep walking until we hit NULL
			walkNode = walkNode->nextWord;
		} // walking down the list

	// We haven't found it, and the calling function wants it deleted
	// WE SHOULD NEVER EXECUTE THIS CODE, so be paranoid and print a message
	fprintf(stderr, "File %s: Line %d. Attempt to delete node %8p from "
					"list %8p when not on list. Deleting anyway.\n",
					__FILE__, __LINE__, wordNode, wordList);
	// delete the node
	DeleteNode(wordNode);
	} // DeleteNodeFromList()

////////////////////////////////////
//        Delete Everything       //
////////////////////////////////////

// delete all nodes in the list
void DeleteAllNodes(WordList *wordList)
	{ // DeleteAllNodes()
	// delete the top node & keep going
	while (wordList->topNode != NULL)
		{ // list still has nodes
		// delete the top node
		DeleteNodeFromList(wordList, wordList->topNode);
		} // list still has nodes
	} // DeleteAllNodes()

// Delete the list itself
void DeleteList(WordList *wordList)
	{ // DeleteList()
	// first get rid of the nodes
	DeleteAllNodes(wordList);

	// now get rid of the list
	free(wordList);
	} // DeleteList()

	////////////////////////////////////
	//          AddNodeSorted()       //
	////////////////////////////////////

// Add a node to the list in sorted posiition
void AddNodeSorted(WordList *wordList, WordNode *wordNode)
	{ // AddNodeSorted()
		// special case for empty list
		if (wordList->topNode == NULL)
		{ // empty list
			// set the new node to point to NULL
			wordNode->nextWord = NULL;

			// set the list to point to the node
			wordList->topNode = wordNode;
		} // empty list

		else if (strcmp(wordNode->name, wordList->topNode->name) < 0)
		{ // before the top node
			// connect it's tail to the existing list
			wordNode->nextWord = wordList->topNode;

			// replace the top node on the list with the new one
			wordList->topNode = wordNode;
		} // before the top node
		else
		{ // somewhere else on the list
			// start at the top of the list
			WordNode *searchNode = wordList->topNode;

			// as long as it has another node
			while (searchNode->nextWord != NULL)
			{ // loop down elements
				if(strcmp(wordNode->name, searchNode->nextWord->name) < 0)
				{ // new node is before the next one
					// attach the next one to it
					wordNode->nextWord = searchNode->nextWord;
					// and attach it in the old one's place
					searchNode->nextWord = wordNode;
					// then return because we are done
					return;
				} // new node is before the next one

					// step to next node
					searchNode = searchNode->nextWord;
			} // loop down elements

			// we only get here if we've run to the end of the list
			// so tie off the end of the node
			wordNode->nextWord = NULL;

			// and set it as the tail end
			searchNode->nextWord = NULL;
			// and set it as the tail end
			searchNode->nextWord = wordNode;
		} // somewhere else on the list

	} // AddNodeSorted()
