/------------------------------------------------/
/------ Name - Christopher Burke ----------------/
/- Course - Procedural Programming Coursework 3 -/
/------------------------------------------------/

------------------------------------------------------------------------------------------------------------
Overview:

My program consists of 5 parts.
-header.h
-main.c
functions.c
-makefile
-timetest.sh
------------------------------------------------------------------------------------------------------------
main.c:

Summary. This is where my main routine exists and contains four arguments or conditions that are meant that perform different actions.

Argument 1. I have an if statement that notifys the user via an error message in terminal, if they do not input enough arguments e.g. ./(Argument 1).

Argument 2. I have another if statement that is triggered if the user inputs two arguments (./getWords) (filename), this will open the a text file and
            using the Linked List structure for dynamic data, will count every instance of every word and record it, and display the results. I
            used the time() function, so it will also display how long it took to execute the program.

Argument 3. I have another if statement that is triggered if the user inputs three arguments (./getWords) (filename) (anything), this will open
            the text file and use a Sorted Linked List structure for dynamic data, and just like Argument 2 will count every instance of every word,
            and record it, and display the results, however this time the data will be sorted alphabetically.


Argument 4. I have created 1 if statement for when the user inputs too many arguments e.g. ./(argument 1) (argument2) (argument3) (arguement 4),
        and display an error message notifying the user of this.

------------------------------------------------------------------------------------------------------------
header.h:

Summary. this contains all my variables, pointers, structures, and function prototypes, that I incorporate in my
         functions.c and main.c file.

------------------------------------------------------------------------------------------------------------
functions.c:

Summary. this file is a .c file and contains all the functions that I used within my program.

filter() - Transforms uppercase characters to lowercase/ deals with the apostrophe conditions.

MakeOPutput() - function just to slim down the code for my closing file and freeing up memory

MakeBuffer() - function that generates the buffer from a text file that you input.#

CreateFiledNode() - Most important function it creates the nodes, which is the word and the word counter.

PrintNode() - Function used in conjunction with PrintList(), to print all words in my list.

DeleteNote() - frees up memory after we've done what we've needed to do.

InitialiseList() - creates a new list for storing words in to.

CountNodes() - The function that I use to generate the amount of unique words that are within my Linked List, not a neccessity.

PrintList() - Prints the list or prints all the nodes out.

AddNode() - generates a new node

FindByKey() - Simplistic way of counting my occurences of my word, goes through the text file, only adding
              what is needed.

SortedFindByKey() - similar to above but uses the sort function to make the list ordered alphabettically.

DeleteEverything() - remove all nodes and free up memory

AddNodeSorted() - Similar to add node, however adds the nodes in an alphabetical order, so performs a check to see it the
                  keyname is higher or lower in the list dependant on it's characters.

------------------------------------------------------------------------------------------------------------
timetest.sh:

Summary. This file is a shell script and will record the time it takes for the program to execute.

How it works: To use this file, just type ./timetest.sh (filename), into the terminal, this will print out
              how long the text file took to list both using the linked list structure and the Sorted list
              structure.

NOTE: If you are prompted with "permission denied" error when trying to execute the shellscript, chmod u+x timetest.sh to
      give you privelleges and then it will work correctly

------------------------------------------------------------------------------------------------------------
makefile:

Summary. This file creates object files for all the .c files, this includes main.c, and functions.c this references them to the header file
          and then compiles each file seperately with one command 'make', you can also remove the object files and the applications by
          using the command (make clean) in terminal.
------------------------------------------------------------------------------------------------------------
How to use:

1. first you must navigate to the correct directory in the terminal. e.g. (cd coursework3/Chris_burke...)

2. Once you're in the correct directory, in terminal type "make" and it should then compile all the files. to check if you'r in the correct
   directory type "ls" and you should see all the files e.g. getWords, functions.o, main.o, readme.txt, header.h, main.c,
   functions.c, timetest.sh and makefile.

3. Once you've run the make command, you can use the program by typing ./getWords (input_file) this will display the unsorted (Linked List approach) that will
   list all the words within the text file and how many occurences each word had, and the time it took to execute the file below.

4. You can also view a sorted list (Sorted Linked List approach) by using 3 arguments instead of two, by typing ./getWords (input_file) (sort), it will
   then list all the words like in the previous step however the words will be sorted alphabetically this time.

------------------------------------------------------------------------------------------------------------
