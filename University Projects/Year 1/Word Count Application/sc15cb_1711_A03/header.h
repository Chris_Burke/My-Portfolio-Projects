///////////////////////////////////////////
//                                       //
//   COMP 1711 Novemeber 2015            //
//                                       //
// header.h: Header file containing all  //
//           variables and functions     //
//           prototyped used within my   //
//           program                     //
//                                       //
///////////////////////////////////////////

//////////////////////////////
//  1. Variables & Pointers //
//////////////////////////////

// Clock start time
clock_t start_t;

// Clock end time
clock_t end_t;

// Clock differentce to measure execution time
double diff_t;

// file pointer Input
FILE *in_file;

// lSize variable for storing file
long lSize;

// buffer pointer
char * buffer;

// word variable to store words
char myWord[1000];

/////////////////////
//  2. Structures  //
/////////////////////

// the actual struct
struct word_struct
{ // struct word_struct
// the key(i.e how we identify it)
char *name;
// the data (i.e. what is stored under the key)
int amount;

// pointer top the next item
struct word_struct *nextWord;
}; // struct word_struct

// typedef to make life easier
typedef struct word_struct WordNode;

// the actual struct
struct word_list
	{ // struct word_list_struct
	// pointer to first node on list
	WordNode *topNode;
	}; // struct word_list_struct
// typedef to have an easy name
typedef struct word_list WordList;

//////////////////////////////
//  3. Function Prototypes  //
//////////////////////////////

// Prints list and free's up memory and closes file
void MakeOutput();

// makes buffer for file
void MakeBuffer();

// Sort nodes in list alphabetically
void AddNodeSorted(WordList *wordList, WordNode *wordNode);

// filter function
void filter();

// Initialise the list to empty
void InitialiseList (WordList *wordList);

// Find out how many nodes in the list
int CountNodes(WordList *wordList);

// Print out list
void PrintList(WordList * wordList);

// Add a node at the beginning of the list
void AddNode (WordList *wordList, WordNode *wordNode);

// Find a specific node in the list by key
WordNode *FindByKey(WordList *wordList, char *keyName);

// Find a specific node in the list by key
WordNode *SortedFindByKey(WordList *wordList, char *keyName);

// Delete A Node
void DeleteNode (WordNode *wordNode);

// Delete a node in the list
void DeleteNodeFromList(WordList *wordList, WordNode *wordNode);

// Delete all nodes in the list
void DeleteAllNodes(WordList *wordList);

// Create a node with a specific name / amount
// by *Copying* the parameter: The calling function
// is responsible for its own strings
WordNode *CreateFilledNode(char *wordor, int owing);

// print out a node
void PrintNode(WordNode *wordNode);
