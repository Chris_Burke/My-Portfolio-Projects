///////////////////////////////////////////
//                                       //
//   COMP 1711 Novemeber 2015            //
//                                       //
// Main.c: code file for the main        //
//         routine producing a linked    //
//         list and sorted list!         //
//                                       //
///////////////////////////////////////////

// include stdlib for malloc(), string for strdup(), stdio for printing,
// time for measuring the time.
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

// include our header file
#include "header.h"

// main routine
int main(int argc, char* argv[]) { // main()

	// Start the clock
	start_t = clock();

////////////////////////////////////////
//  Condition 1. Too fewer arguments  //
////////////////////////////////////////

	// If user inputted 1 argument display error message
	if(argc == 1) { // User inputted one command line argument
		printf("=====================================================\n");
		printf("ERROR - User inputted insufficent amount of arguments\n");
		printf("For a Linked list ./getWords (filename)\n");
		printf("For a Sorted List ./getword (filename) (sort)\n");
		printf("=====================================================\n");
		return -1;
		} // User inputted one command line argument

////////////////////////////////////////
//  Condition 2. Linked List approach //
////////////////////////////////////////

	// If User inputted two command line arguments
	else if(argc == 2) { // User inputted two command line arguments

		// create a variable for open the file
		in_file = fopen(argv[1],"rwb");

		// Call make buffer function, does all file operations and conditions.
		MakeBuffer();

		// create a wordlist
		WordList myList;

		// initialise the list
		InitialiseList(&myList);

		// calls filter function
		filter();

		// create a pointer called token
		char *token;

		// create a variable j for indexing through file
		int j;
		// loops through the buffer and seperates the characters below:
		//-------------------------------------------------------------
		// All Numbers 0-9
		// Comma (,)	// full stop (.)	// Exclamation mark (!)	// Question mark (?)
		// semi colon (;)	// quotation marks ("" \x22)	// hyphen (-)	// Brackets () []
		// Asterisk (*)	// Pound sign (£)	dollar sign ($)	// Underscore (_)
		// New line (\n)	// new tab (\t)	// Space ( )
		for(j = 0 ; (token = strsep(&buffer, "0123456789,.!?;-()[]*£$_ \n\t\x22")) ;j++){ // seperate characters

			if(strlen(token)!=0){ // if the file hasn't hit a null terminator

				// call function FindByKey(),l to create the nodes.
				FindByKey(&myList, token);

				} // if the file hasn't hit a null terminator

			} // seperate characters

		// Call function PrintList(), to print the nodelist
		PrintList(&myList);

		// Call make output function, closes file and formats readability
		MakeOutput();

	} // User inputted two command line arguments

////////////////////////////////////////
//  Condition 3. Sorted List approach //
////////////////////////////////////////

	// If User inputted three command line arguments
	else if (argc == 3) { // User inputted three command line arguments

		// create a variable for open the file
		in_file = fopen(argv[1],"rwb");

		// Call make buffer function, does all file operations and conditions.
		MakeBuffer();

		// create a wordlist
		WordList myList;

		// initialise the list
		InitialiseList(&myList);

		// calls filter function
		filter();

		// create a pointer called token
		char *token;

		// create a variable j for indexing through file
		int j;
		// loops through the buffer and seperates the characters below:
		//-------------------------------------------------------------
		// All Numbers 0-9
		// Comma (,)	// full stop (.)	// Exclamation mark (!)	// Question mark (?)
		// semi colon (;)	// quotation marks ("" \x22)	// hyphen (-)	// Brackets () []
		// Asterisk (*)	// Pound sign (£)	dollar sign ($)	// Underscore (_)
		// New line (\n)	// new tab (\t)	// Space ( )
		for(j = 0 ; (token = strsep(&buffer, "0123456789,.!?;-()[]*£$_ \n\t\x22")) ;j++){ // seperate characters

			if(strlen(token)!=0){ // if the file hasn't hit a null terminator

				// call function FindByKey(),l to create the nodes.
				SortedFindByKey(&myList, token);
				} // if the file hasn't hit a null terminator
			} // seperate characters

		// Call function PrintList(), to print the nodelist
		PrintList(&myList);

		// Call make output function, closes file and formats readability
		MakeOutput();

	} // User inputted three command line arguments

////////////////////////////////////////
//  Condition 4. Too many arguments //
////////////////////////////////////////

// If user inputted more than three comand line arguments, display an error message.
	else { // Otherwise notify user with error message
		printf("=============================================\n");
		printf("ERROR - User inputted too many arguments\n");
		printf("For a Linked list ./getWords (filename)\n");
		printf("For a Sorted List ./getword (filename) (sort)\n");
		printf("=============================================\n");
		return -1;
		} // Otherwise notify user with error message

		// stop the clock timer
		end_t = clock();

		diff_t = difftime(end_t, start_t);

		printf("Execution Time = %f seconds\n\n", diff_t);

	return 0;
} // main()
